--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.8
-- Dumped by pg_dump version 9.6.5

-- Started on 2018-01-12 16:32:47 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 21794)
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE app_configuration OWNER TO gazelle;

--
-- TOC entry 190 (class 1259 OID 21891)
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_configuration_id_seq OWNER TO gazelle;

--
-- TOC entry 182 (class 1259 OID 21802)
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE cmn_home OWNER TO gazelle;

--
-- TOC entry 191 (class 1259 OID 21893)
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_home_id_seq OWNER TO gazelle;

--
-- TOC entry 192 (class 1259 OID 21895)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO gazelle;

--
-- TOC entry 186 (class 1259 OID 21828)
-- Name: pxy_abstract_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE pxy_abstract_message (
    pxy_abstract_message_type character varying(31) NOT NULL,
    id integer NOT NULL,
    date_received timestamp without time zone,
    from_ip character varying(255),
    index character varying(255),
    index_int integer,
    local_port integer,
    messagereceived bytea,
    proxy_port integer,
    proxy_side integer,
    remote_port integer,
    result_oid bytea,
    to_ip character varying(255),
    hl7_messagetype character varying(255),
    hl7_version character varying(255),
    appname text,
    facility integer,
    hostname text,
    messageid text,
    payload text,
    procid text,
    severity integer,
    tag text,
    "timestamp" text,
    http_headers bytea,
    http_messagetype character varying(255),
    dicom_filecommandset bytea,
    dicom_filetransfertsyntax text,
    dicom_affectedsopclassuid character varying(255),
    dicom_commandfield character varying(255),
    dicom_requestedsopclassuid character varying(255),
    connection_id integer
);


ALTER TABLE pxy_abstract_message OWNER TO gazelle;

--
-- TOC entry 187 (class 1259 OID 21836)
-- Name: pxy_connection; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE pxy_connection (
    id integer NOT NULL,
    uuid character varying(255)
);


ALTER TABLE pxy_connection OWNER TO gazelle;

--
-- TOC entry 193 (class 1259 OID 21897)
-- Name: pxy_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pxy_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pxy_message_id_seq OWNER TO gazelle;

--
-- TOC entry 183 (class 1259 OID 21810)
-- Name: tm_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_configuration (
    id integer NOT NULL,
    host character varying(255),
    name text,
    port integer NOT NULL,
    proxyport integer NOT NULL,
    tmid integer NOT NULL,
    type integer,
    testinstance_id integer
);


ALTER TABLE tm_configuration OWNER TO gazelle;

--
-- TOC entry 184 (class 1259 OID 21818)
-- Name: tm_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_step (
    id integer NOT NULL,
    date timestamp without time zone,
    stepindex integer,
    tmid integer NOT NULL,
    message_id integer,
    testinstance_id integer
);


ALTER TABLE tm_step OWNER TO gazelle;

--
-- TOC entry 188 (class 1259 OID 21841)
-- Name: tm_step_receiver; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_step_receiver (
    tm_step_id integer NOT NULL,
    receivers_id integer NOT NULL
);


ALTER TABLE tm_step_receiver OWNER TO gazelle;

--
-- TOC entry 189 (class 1259 OID 21844)
-- Name: tm_step_sender; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_step_sender (
    tm_step_id integer NOT NULL,
    senders_id integer NOT NULL
);


ALTER TABLE tm_step_sender OWNER TO gazelle;

--
-- TOC entry 185 (class 1259 OID 21823)
-- Name: tm_testinstance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_testinstance (
    id integer NOT NULL,
    date timestamp without time zone,
    tmid integer NOT NULL
);


ALTER TABLE tm_testinstance OWNER TO gazelle;

--
-- TOC entry 2061 (class 2606 OID 21801)
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- TOC entry 2065 (class 2606 OID 21809)
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- TOC entry 2103 (class 2606 OID 21835)
-- Name: pxy_abstract_message pxy_abstract_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pxy_abstract_message
    ADD CONSTRAINT pxy_abstract_message_pkey PRIMARY KEY (id);


--
-- TOC entry 2112 (class 2606 OID 21840)
-- Name: pxy_connection pxy_connection_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pxy_connection
    ADD CONSTRAINT pxy_connection_pkey PRIMARY KEY (id);


--
-- TOC entry 2074 (class 2606 OID 21817)
-- Name: tm_configuration tm_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_configuration
    ADD CONSTRAINT tm_configuration_pkey PRIMARY KEY (id);


--
-- TOC entry 2081 (class 2606 OID 21822)
-- Name: tm_step tm_step_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step
    ADD CONSTRAINT tm_step_pkey PRIMARY KEY (id);


--
-- TOC entry 2084 (class 2606 OID 21827)
-- Name: tm_testinstance tm_testinstance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_testinstance
    ADD CONSTRAINT tm_testinstance_pkey PRIMARY KEY (id);


--
-- TOC entry 2063 (class 2606 OID 21848)
-- Name: app_configuration uk_20rnkdjn5jvlvmsb5f7io4b1o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT uk_20rnkdjn5jvlvmsb5f7io4b1o UNIQUE (variable);


--
-- TOC entry 2067 (class 2606 OID 21850)
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- TOC entry 2059 (class 1259 OID 21935)
-- Name: app_configuration_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX app_configuration_id_index ON app_configuration USING btree (id);


--
-- TOC entry 2085 (class 1259 OID 21906)
-- Name: appname_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX appname_index ON pxy_abstract_message USING btree (appname);


--
-- TOC entry 2086 (class 1259 OID 21913)
-- Name: connection_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX connection_id_index ON pxy_abstract_message USING btree (connection_id);


--
-- TOC entry 2087 (class 1259 OID 21912)
-- Name: date_received_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX date_received_index ON pxy_abstract_message USING btree (date_received);


--
-- TOC entry 2088 (class 1259 OID 21932)
-- Name: dicom_affectedsopclassuid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_affectedsopclassuid_index ON pxy_abstract_message USING btree (dicom_affectedsopclassuid);


--
-- TOC entry 2089 (class 1259 OID 21930)
-- Name: dicom_commandfield_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_commandfield_index ON pxy_abstract_message USING btree (dicom_commandfield);


--
-- TOC entry 2090 (class 1259 OID 21933)
-- Name: dicom_filecommandset_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_filecommandset_index ON pxy_abstract_message USING btree (dicom_filecommandset);


--
-- TOC entry 2091 (class 1259 OID 21934)
-- Name: dicom_filetransfertsyntax_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_filetransfertsyntax_index ON pxy_abstract_message USING btree (dicom_filetransfertsyntax);


--
-- TOC entry 2092 (class 1259 OID 21931)
-- Name: dicom_requestedsopclassuid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX dicom_requestedsopclassuid_index ON pxy_abstract_message USING btree (dicom_requestedsopclassuid);


--
-- TOC entry 2093 (class 1259 OID 21907)
-- Name: facility_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX facility_index ON pxy_abstract_message USING btree (facility);


--
-- TOC entry 2094 (class 1259 OID 21899)
-- Name: from_ip_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX from_ip_index ON pxy_abstract_message USING btree (from_ip);


--
-- TOC entry 2068 (class 1259 OID 21917)
-- Name: host_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX host_index ON tm_configuration USING btree (host);


--
-- TOC entry 2095 (class 1259 OID 21908)
-- Name: hostname_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX hostname_index ON pxy_abstract_message USING btree (hostname);


--
-- TOC entry 2110 (class 1259 OID 21928)
-- Name: id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX id_index ON pxy_connection USING btree (id);


--
-- TOC entry 2096 (class 1259 OID 21914)
-- Name: index_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX index_index ON pxy_abstract_message USING btree (index);


--
-- TOC entry 2097 (class 1259 OID 21915)
-- Name: index_int_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX index_int_index ON pxy_abstract_message USING btree (index_int);


--
-- TOC entry 2098 (class 1259 OID 21900)
-- Name: local_port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX local_port_index ON pxy_abstract_message USING btree (local_port);


--
-- TOC entry 2077 (class 1259 OID 21927)
-- Name: message_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX message_id_index ON tm_step USING btree (message_id);


--
-- TOC entry 2099 (class 1259 OID 21909)
-- Name: messageid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX messageid_index ON pxy_abstract_message USING btree (messageid);


--
-- TOC entry 2069 (class 1259 OID 21918)
-- Name: name_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX name_index ON tm_configuration USING btree (name);


--
-- TOC entry 2070 (class 1259 OID 21919)
-- Name: port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX port_index ON tm_configuration USING btree (port);


--
-- TOC entry 2100 (class 1259 OID 21901)
-- Name: proxy_port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX proxy_port_index ON pxy_abstract_message USING btree (proxy_port);


--
-- TOC entry 2101 (class 1259 OID 21902)
-- Name: proxy_side_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX proxy_side_index ON pxy_abstract_message USING btree (proxy_side);


--
-- TOC entry 2071 (class 1259 OID 21920)
-- Name: proxyport_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX proxyport_index ON tm_configuration USING btree (proxyport);


--
-- TOC entry 2104 (class 1259 OID 21916)
-- Name: pxy_abstract_message_type_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX pxy_abstract_message_type_index ON pxy_abstract_message USING btree (pxy_abstract_message_type);


--
-- TOC entry 2105 (class 1259 OID 21903)
-- Name: remote_port_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX remote_port_index ON pxy_abstract_message USING btree (remote_port);


--
-- TOC entry 2106 (class 1259 OID 21904)
-- Name: result_oid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX result_oid_index ON pxy_abstract_message USING btree (result_oid);


--
-- TOC entry 2107 (class 1259 OID 21910)
-- Name: severity_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX severity_index ON pxy_abstract_message USING btree (severity);


--
-- TOC entry 2078 (class 1259 OID 21924)
-- Name: stepindex_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX stepindex_index ON tm_step USING btree (stepindex);


--
-- TOC entry 2072 (class 1259 OID 21923)
-- Name: testinstance_id_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX testinstance_id_index ON tm_configuration USING btree (testinstance_id);


--
-- TOC entry 2079 (class 1259 OID 21926)
-- Name: testinstance_id_index1; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX testinstance_id_index1 ON tm_step USING btree (testinstance_id);


--
-- TOC entry 2108 (class 1259 OID 21911)
-- Name: timestamp_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX timestamp_index ON pxy_abstract_message USING btree ("timestamp");


--
-- TOC entry 2075 (class 1259 OID 21921)
-- Name: tmid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX tmid_index ON tm_configuration USING btree (tmid);


--
-- TOC entry 2082 (class 1259 OID 21925)
-- Name: tmid_index1; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX tmid_index1 ON tm_step USING btree (tmid);


--
-- TOC entry 2109 (class 1259 OID 21905)
-- Name: to_ip_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX to_ip_index ON pxy_abstract_message USING btree (to_ip);


--
-- TOC entry 2076 (class 1259 OID 21922)
-- Name: type_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX type_index ON tm_configuration USING btree (type);


--
-- TOC entry 2113 (class 1259 OID 21929)
-- Name: uuid_index; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX uuid_index ON pxy_connection USING btree (uuid);


--
-- TOC entry 2120 (class 2606 OID 21881)
-- Name: tm_step_sender fk_36lglup251ovijenl5ra7vy3k; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step_sender
    ADD CONSTRAINT fk_36lglup251ovijenl5ra7vy3k FOREIGN KEY (senders_id) REFERENCES tm_configuration(id);


--
-- TOC entry 2117 (class 2606 OID 21866)
-- Name: pxy_abstract_message fk_39nerc9uwfwuh5ukcpmw5hob; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pxy_abstract_message
    ADD CONSTRAINT fk_39nerc9uwfwuh5ukcpmw5hob FOREIGN KEY (connection_id) REFERENCES pxy_connection(id);


--
-- TOC entry 2119 (class 2606 OID 21876)
-- Name: tm_step_receiver fk_7nr9rxx6dmi1kdmd90phfg8qt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step_receiver
    ADD CONSTRAINT fk_7nr9rxx6dmi1kdmd90phfg8qt FOREIGN KEY (tm_step_id) REFERENCES tm_step(id);


--
-- TOC entry 2118 (class 2606 OID 21871)
-- Name: tm_step_receiver fk_8krrcr8hvb6ukbh64fega07y7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step_receiver
    ADD CONSTRAINT fk_8krrcr8hvb6ukbh64fega07y7 FOREIGN KEY (receivers_id) REFERENCES tm_configuration(id);


--
-- TOC entry 2115 (class 2606 OID 21856)
-- Name: tm_step fk_d8w4qah6akmfdb99p2089lphp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step
    ADD CONSTRAINT fk_d8w4qah6akmfdb99p2089lphp FOREIGN KEY (message_id) REFERENCES pxy_abstract_message(id);


--
-- TOC entry 2116 (class 2606 OID 21861)
-- Name: tm_step fk_h0mxsfukadxicvpcs3gxlqnm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step
    ADD CONSTRAINT fk_h0mxsfukadxicvpcs3gxlqnm FOREIGN KEY (testinstance_id) REFERENCES tm_testinstance(id);


--
-- TOC entry 2114 (class 2606 OID 21851)
-- Name: tm_configuration fk_hf8i3isfu7ha2655vv85wrleb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_configuration
    ADD CONSTRAINT fk_hf8i3isfu7ha2655vv85wrleb FOREIGN KEY (testinstance_id) REFERENCES tm_testinstance(id);


--
-- TOC entry 2121 (class 2606 OID 21886)
-- Name: tm_step_sender fk_ljcxwmqx0atitsxd25sks1nxj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_step_sender
    ADD CONSTRAINT fk_ljcxwmqx0atitsxd25sks1nxj FOREIGN KEY (tm_step_id) REFERENCES tm_step(id);
